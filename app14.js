const themeBtn = document.querySelector(".theme");
const background = document.querySelector("body")
const title = document.querySelector("signin-title");
const signForm = document.querySelector(".signin");
const form = document.querySelector(".signin-form");
const inputs = Array.from(document.querySelectorAll("input"));
const submitBtn = document.querySelector(".signin-form-submit"); 

const arrElements = [signForm, form, inputs, submitBtn, background]

function changeTheme(arr) {  
    arr.forEach((element) => { 
        if (Array.isArray(element)) {
            element.forEach((el) => {
                el.classList.toggle("color-theme") 
            });
        } else {
            element.classList.toggle("color-theme")
        } 
    }); 
} 
 
themeBtn.addEventListener('click', () => {
    document.body.classList.toggle("dark-mode") 
    changeTheme(arrElements); 
    if (localStorage.getItem("colorTheme") === "dark-mode") {
        localStorage.setItem("colorTheme", "light-mode") 
    } else {
        localStorage.setItem("colorTheme", "dark-mode")
    }
});
if (localStorage.getItem("colorTheme") === "light-mode") {
    changeTheme(arrElements)
}
